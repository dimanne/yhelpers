# ======================================== Header ========================================
function impl_mixin_deps
   set this_dir (dirname (realpath (status current-filename)))
   echo $this_dir/../20_fish/mixins/common_fish_setup.fish
   echo $this_dir/../20_fish/mixins/bat.fish
end
function impl_config_fish
end
function impl_fish_home_path
   echo $HOME/docker_mounts/yellow_vpn
end
# ======================================== Other ========================================




function impl_ybd_paths_add_entry --argument-names key arguments
   impl_set_dict_key ybd_paths $key (impl_omit_first_n_args 1 $argv)
   echo $key
end
function impl_ybd_paths_get_entry --argument-names key
   impl_get_dict_key ybd_paths $key
end
function impl_ybd_git_repo
   echo $HOME/devel/ybd
end
function impl_ybd_build_dir
   echo $HOME/devel/build
end
function impl_populate_ybd_paths_hash
   impl_ybd_paths_add_entry (impl_ybd_build_dir) (impl_ybd_build_dir)/ybd/postgresql/build/db/bin
   impl_ybd_paths_add_entry (impl_ybd_git_repo)  (impl_ybd_git_repo)/postgresql/build/db/bin (impl_ybd_git_repo)/lime/build/bin
end



function impl_ybd_remove_all_dynamic_paths
   # https://stackoverflow.com/questions/39754257/how-can-i-delete-a-specific-variable-from-my-path-environment-variable-using-fis
   # https://superuser.com/questions/776008/how-to-remove-a-path-from-path-variable-in-fish
   for key in (impl_populate_ybd_paths_hash)
      for path in (impl_ybd_paths_get_entry $key)
          echo "Considering $path for removal..."
          set -l index (contains -i -- $path $fish_user_paths)
          if test -n "$index"
              echo "Found $path at index $index in the list: $fish_user_paths"
              set --erase fish_user_paths[$index]
              echo "Result after erasing: $fish_user_paths"
          end
      end
   end
end
function impl_add_appropriate_paths_in_fish_user_paths --argument-names dir_longer
   for key_shorter in (impl_populate_ybd_paths_hash)
      set -l in_the_key (string match -e -- $key_shorter $dir_longer)
      if test -n "$in_the_key"
         set -l to_add_paths (impl_ybd_paths_get_entry $key_shorter)
         # echo "We are in dir: $dir_longer, which is in $key_shorter => adding paths: $to_add_paths"
         for to_add in $to_add_paths
            set -U fish_user_paths $to_add $fish_user_paths
         end
      end
   end
end
function impl_ybd_update_path --on-variable PWD
   # echo "Moved to $PWD"
   set --erase fish_user_paths
   # impl_ybd_remove_all_dynamic_paths
   impl_add_appropriate_paths_in_fish_user_paths $PWD
   # set -U fish_user_paths ~/devel/ygem $fish_user_paths
   set -x fish_user_paths ~/devel/ygem $fish_user_paths
end



function impl_get_YBARCBUILD  --argument-names git_repo arc_build_dir
   if test (count $argv) -eq 0
      set git_repo (impl_ybd_git_repo)
      set arc_build_dir (impl_ybd_build_dir)/archimedes
   end
   echo (realpath --relative-to=$git_repo/archimedes $arc_build_dir)
end



function bl
   argparse 'v-var=' 'p/parallel' -- $argv
   if set -q _flag_var
      set variant $_flag_var
   else
      set variant Debug
   end
   if test $argv[1] = "re"
      set rebuild ""
   end

   set -l git_repo (impl_ybd_git_repo)
   set -l build_dir (impl_ybd_build_dir)

   if contains arc $argv || contains all $argv
      echo "" > /tmp/bl-arc.fish
      if set -q rebuild
         echo "Rebuilding archimedes with variant: $variant"
         echo "
            rm -rf $build_dir/archimedes
            mkdir -p $build_dir/archimedes
         " >> /tmp/bl-arc.fish
      else
         echo "Building archimedes with variant: $variant"
      end
      echo "
         cd $build_dir/archimedes
         cmake -DVARIANT=$variant -Wno-dev $git_repo/archimedes
         make -j 25
      " >> /tmp/bl-arc.fish

      if set -q _flag_parallel
         /usr/bin/fish /tmp/bl-arc.fish > /tmp/bl-arc.log 2>&1 &
         set pids_to_wait $last_pid $pids_to_wait
         echo "archimedes build has been started, check logs: tail -F /tmp/bl-arc.log"
      else
         /usr/bin/fish /tmp/bl-arc.fish
      end
   end

   if contains pg $argv || contains all $argv
      echo "" > /tmp/bl-pg.fish

      if set -q rebuild
         echo "Rebuilding Postgresql with variant: $variant"
         echo "
         rm -rf $git_repo/postgresql/build
         " >> /tmp/bl-pg.fish
      else
         echo "Building Postgresql with variant: $variant"
      end

      echo "
      mkdir -p $git_repo/postgresql/build
      cd $git_repo/postgresql
      " >> /tmp/bl-pg.fish

      if set -q rebuild
         echo "
         ./ybpg_configure
         " >> /tmp/bl-pg.fish
      else
         echo "
         [ -f build/GNUmakefile ] || ./ybpg_configure
         " >> /tmp/bl-pg.fish
      end

      echo "
      set -x VARIANT $variant
      set -x YBD_PROFILE DEV
      cd $git_repo/postgresql/build/src/port && make -j (math (nproc) - 1) all
      cd $git_repo/postgresql/build/src/backend && make -j (math (nproc) - 1) all
      cd $git_repo/postgresql/build/src/timezone && make -j (math (nproc) - 1) all
      cd $git_repo/postgresql/build && make -j (math (nproc) - 1) all
      cd $git_repo/postgresql/build && make -j (math (nproc) - 1) install
      " >> /tmp/bl-pg.fish

      if set -q _flag_parallel
         /usr/bin/fish /tmp/bl-pg.fish > /tmp/bl-pg.log 2>&1 &
         set pids_to_wait $last_pid $pids_to_wait
         echo "postgresql build has been started, check logs: tail -F /tmp/bl-pg.log"
      else
         /usr/bin/fish /tmp/bl-pg.fish
      end
   end

   if contains lime $argv || contains all $argv
      echo "(Re-)building lime"
      echo "" > /tmp/bl-lime.fish
      if set -q rebuild
         set -l clean_flag clean
      else
         set -l clean_flag ""
      end
      echo "
      cd $git_repo
      mvn -T (math (nproc) - 1) -P DEV -DskipTests -Dmaven.test.skip=true clean install
      " > /tmp/bl-lime.fish

      if set -q _flag_parallel
         /usr/bin/fish /tmp/bl-lime.fish > /tmp/bl-lime.log 2>&1 &
         set pids_to_wait $last_pid $pids_to_wait
         echo "lime build has been started, check logs: tail -F /tmp/bl-lime.log"
      else
         /usr/bin/fish /tmp/bl-lime.fish
      end
   end

   if set -q pids_to_wait
      # set up a handler to be triggered if "wait $pids_to_wait" is cancelled mid-run
      function on_premature_exit --on-job-exit %self --inherit-variable pids_to_wait
         functions -e on_premature_exit  # erase to prevent recursively calling itself when it exits
         echo "Killing pids: $pids_to_wait..."
         kill -9 $pids_to_wait
         echo "done"
      end
      echo "Waiting for PIDs to finish: $pids_to_wait"
      wait $pids_to_wait
      functions -e on_premature_exit
   end

   cd . # Repopulate paths

   echo "Done"
   set -l YBARCBUILD (impl_get_YBARCBUILD $git_repo $build_dir/archimedes)
   echo "You can now start DB with: set -x YBARCBUILD $YBARCBUILD; ybstop && ybinit-dev && ybstart --all --provision"
end



function erase-lime-logs
   set -l git_repo (impl_ybd_git_repo)
   echo "" > $git_repo/lime/build/data/log/cc.log
   echo "" > $git_repo/lime/build/data/log/lime-output.log
   echo "" > $git_repo/lime/build/data/log/lime-sanitized.log
   echo "" > $git_repo/lime/build/data/log/lime-status.log
   echo "" > $git_repo/lime/build/data/log/lime.log
end



function prep-cc-logs --argument-names src_dir dst_dir
   if test -z "$src_dir" || test -z "$dst_dir"
      echo "Usage: prep-cc-logs $HOME/devel/ybd/lime/build/data/log $HOME/devel/row-packet"
      return
   end

   touch $dst_dir/cc.cc

   echo '#include "/home/dimanne/devel/ybd/archimedes/common/types2/Y2Types.h"
#include "/home/dimanne/devel/ybd/archimedes/ee/ExprCtx.h"
#include "/home/dimanne/devel/ybd/archimedes/ee/LIMEQuery.h"
#include "/home/dimanne/devel/ybd/archimedes/ee/QueryInjector.h"
#include "/home/dimanne/devel/ybd/archimedes/ee/QueryInjectorFactory.h"
#include "/home/dimanne/devel/ybd/archimedes/ee/base_nodes/ExecutionNode.h"
#include "/home/dimanne/devel/ybd/archimedes/ee/injectors/SimpleAggregator.h"
' > $dst_dir/cc.cc
   cat $src_dir/cc.log >> $dst_dir/cc.cc
   # "2021-08-29T17:39:06.022Z | "
   awk -i inplace '{if($0 ~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}Z \| /) {print "// " $0} else { print $0 }}' $dst_dir/cc.cc
end



function reinit-ybd --argument-names mode --description 'mode=[rel, deb, asan_rel, asan_deb]'
   sudo pkill -9 -f "postgres"
   sudo pkill -9 -f "(/usr/bin/java .*lime.*|/bin/bash .*lime.*)"
   sudo pkill -9 -f ".*lldb .*worker"
   # sudo rmrf $HOME/devel/build/ydata/
   mkdir -p $HOME/devel/build/ydata
   sudo find $HOME/devel/build/ydata/ -mindepth 1 -delete
   set -x YBARCBUILD (impl_get_YBARCBUILD) # Only for detect_config (which is called from ybstop) to succeed
   # Fix ybstatus to ignore defunct procecess in docker:
   awk -i inplace 'BEGIN {s = "grep -v \"<defunct>\""} {if($0 ~ s) print $0; else print gensub(/\$PS \|/, "$PS | "s" |", "g", $0); }' (which ybstatus)
   # ybstop &&
   ybinit-dev
   sudo mkdir -p $HOME/devel/build/ydata/
   sudo touch $HOME/devel/build/ydata/arcBootLog-Worker.txt

   set -l existing_options (grep "compiler.clang++.compileObjOptions" (impl_ybd_git_repo)/lime/build/conf/lime.properties)
   if test -n $existing_options
      if string match -q -- "*-O0;-g*" $existing_options # we have -O0;-g somewhere in the options
         if test $mode = "rel"
            sed -i 's/-O0;-g//g' (impl_ybd_git_repo)/lime/build/conf/lime.properties
         end
         # Otherwise, do not do anything
      else # There is no -O0;-g in the options
         if test $mode = "deb"
            awk -i inplace 'BEGIN {s = "compiler\\\\.clang\\\\+\\\\+\\\\.compileObjOptions"} {if($0 !~ s) print $0; else print $0 ";-O0;-g"; }' (impl_ybd_git_repo)/lime/build/conf/lime.properties
         end
      end
   end

   # Fix: ERROR:  OutOfDirectMemoryError
   sed -i 's/^ *# * io.netty.allocator.numDirectArenas=0$/io.netty.allocator.numDirectArenas=0/g' (impl_ybd_git_repo)/lime/build/etc/system.properties
   sed -i 's/^ *# * io.netty.noPreferDirect=true$/io.netty.noPreferDirect=true/g' (impl_ybd_git_repo)/lime/build/etc/system.properties
end



function lime --argument-names archimedes_build_dir_name lime_args
   set -l fish_trace 1
   set -l git_repo (impl_ybd_git_repo)
   set -l build_dir (impl_ybd_build_dir)

   set -x EXTRA_JAVA_OPTS -Darchimedes.dir.name=(impl_get_YBARCBUILD $git_repo $build_dir/$archimedes_build_dir_name) -XX:MaxDirectMemorySize=4096M # Used to find pch
   set -x TERM xterm-color
   set -x JAVA_MIN_MEM 4096M
   set -x JAVA_MAX_MEM 12288M
   {$git_repo}/lime/build/bin/lime (impl_omit_first_n_args 1 $argv)
end
# Completion for att, taken from http://fishshell.com/docs/current/index.html#completion-own
complete -c lime -f # Disable file completions
complete -c lime -a "(ls -d (impl_ybd_build_dir)/archimedes* | xargs -L1 basename)" # Suggest dirs starting with archimedes from build dir



# Extract worker from installer:
# cat $HOME/devel/y-skew/gen2-crash/ybd-5.4.0-37047-release-custom | awk 'BEGIN{in_data = 0} { if(in_data == 1) in_data = 2; if(in_data == 0 && $0 ~ /PKG_DATA_[0-9]+ *= b"""/){in_data = 1;} else if(in_data == 2 && $0 ~ /"""$/) {in_data = 0; print substr($0, 0, length($0) - 3)}  if(in_data == 2) print $0; } END{}' | base64 -d | tar -zxv -C .
# tar -xf $HOME/devel/y-skew/gen2-crash/ybd-5.4.0-37047/install_files/bladeboot/gen2/worker-Gen2-5.4.0.20211112122842.tar.bz2 -C $HOME/devel/y-skew/gen2-crash/ybd-5.4.0-37047/install_files/bladeboot/gen2/
# Result:
# /home/dimanne/devel/y-skew/gen2-crash/ybd-5.4.0-37047/install_files/bladeboot/gen2/worker

# Create core file
# ~/devel/build/archimedes-qtc-Gen2-rel/tools/minidump-2-core ~/devel/y-skew/gen2-crash/release-5.4.0-20211112122842-gen2-ubuntu-00000000-0000-0000-0000-38b8ebd20e10-2021-11-16T14_41_29.576Z.dmp >  ~/devel/y-skew/gen2-crash/14_41_29.576Z.core.out

# Run
# lldb /home/dimanne/devel/y-skew/gen2-crash/ybd-5.4.0-37047/install_files/bladeboot/gen2/worker -c ~/devel/y-skew/gen2-crash/14_41_29.576Z.core.out
