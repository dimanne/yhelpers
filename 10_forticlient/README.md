### Goal

Start `forticlient` in a container.

Essentially, everything in this directory is required to automate the only command:

```bash
$ /opt/forticlient/vpn --server globalvpn.yellowbrick.com:8443 --user dmitry.sokolov@yellowbrick.com --password
```

### Public interface / API of the container

1. Build it with something like this: `docker build --rm=true -t <your_name_of_image> --build-arg base_image=<base_image_such_as_ubuntu_or_whatever_you_need> .`

2. Create file that would contain credentials: `~/docker_mounts/forticlient/credentials.sh`:

   ```
   FORTI_PASS="..."
   FORTI_SERVER_PORT="globalvpn.yellowbrick.com:8443"
   FORTI_USERNAME="dmitry.sokolov@yellowbrick.com"
   ```

3. Mount it in `/vpn/`: `--volume $HOME/docker_mounts/forticlient/credentials.sh:/vpn/credentials.sh`.

4. Start container with `--security-opt seccomp=$HOME/devel/scripts/docker/10_forticlient/seccomp.json`.

5. Once in the container, connect to vpn: `/vpn/forti_start.sh`.

6. Now you can start Chrome in the container and it should have access to internal resources.

7. Optionally, you can simplify start up of the container via aliases/functions, for example in fish:
   ```
   $ functions dev_yellow_vpn
   # Defined in /home/dimanne/.config/fish/conf.d/host_impedance.fish @ line 38
   function dev_yellow_vpn
      docker run (impl_docker_rm_nvidia_header)  (impl_docker_term) (impl_docker_x11 $HOME) \
      (impl_docker_generate_name_arg yellow_vpn) (impl_docker_yellow_vpn_mounts) \
      (impl_docker_generate_scard_devices_arg) \
      --device /dev/net/tun \
      --security-opt seccomp=$HOME/devel/scripts/docker/10_forticlient/seccomp.json \
      --volume $HOME/devel:$HOME/devel:rw \
      --cap-add NET_ADMIN --cap-add CAP_SYS_ADMIN \
      (whoami)/dev_yellow_vpn /bin/fish
   end
   ```

### Internals

Read this section only if you need to understand how and why the container was made this way.

- `Dockerfile` downloads `forticlient` from official https://links.fortinet.com/forticlient/deb/vpnagent.
- `forti_start.sh` is the main entry-point. It prepares environment for `forticlient` and invokes `expect` script - `forti_start.exp`.
- `forti_start.exp` - expect script, the only purpose of which is to respond to the `password` prompt of CLI `forticlient` - `/opt/forticlient/vpn` and enter your password for you (takes it from `FORTI_PASS` variable from `credentials.sh`).

##### `/opt/forticlient/vpn`'s requirements

In short: `forticlient` heavily relies on `nmcli` to make (primarily DNS-related) changes to network interfaces/settings. The problem is that `nmcli` does not work in docker (it errors out with `strictly unmanaged interface `). Below is a description of how we mock/fake required parts of `nmcli` so that `forticlient` be happy.

First of all, here is a command that we would normally use to start it manually: `/opt/forticlient/vpn --server globalvpn.yellowbrick.com:8443 --user dmitry.sokolov@yellowbrick.com --password`. It was discovered that:

1. It uses a couple of syscalls (`keyctl` and `add_key`) that are blacklisted in the default docker's seccomp profile. So, we use our custom seccomp profile that is based on the default one, in which the two syscalls are added. Need to add this option to `docker run`: `--security-opt seccomp=$HOME/devel/scripts/docker/10_forticlient/seccomp.json`.

2. It uses `pidof NetworkManager`, hence we need:

   - either spawn an instance of real `NetworkManager`:

      ```bash
      mkdir -p /var/run/dbus && dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address
      apt install network-manager && NetworkManager --no-daemon
      ```

   - or create our own C++ binary that could just present (sleep forever).

   But since `NetworkManager/nmcli` is going to be useless anyway (see below), we implement the second option - see `NetworkManager.cpp` - it is compiled in Dockerfile.

3. Once `NetworkManager/nmcli` is setup, `forticlient` uses `nmcli` to setup VPN:

   ```bash
   root@eb179915178f:~# cat /root/nmcli-invocations.txt
   nmcli --fields GENERAL.CONNECTION device show eth0
   # parses output via "/usr/bin/perl", "-n", "-e/GENERAL\\.CONNECTION:\\s+(.*)/ "
   nmcli --fields ipv4.ignore-auto-dns connection show <DimanNe-GoogleWiFi>
   # parses output via "/usr/bin/perl", "-n", "-e/ipv4\\.ignore-auto-dns:\\s+(.*)"
   nmcli --fields ip4.dns device show eth0
   nmcli --fields ipv4.dns-search connection show <DimanNe-GoogleWiFi>
   nmcli connection modify DimanNe-GoogleWiFi ipv4.ignore-auto-dns yes
   nmcli connection modify DimanNe-GoogleWiFi ipv4.dns "10.10.10.20 10.10.10.21"
   nmcli connection modify DimanNe-GoogleWiFi ipv4.dns-search "yellowbrick.io slc.yellowbrick.io"
   nmcli device reapply eth0
   nmcli connection modify vpn ipv4.dns 10.10.10.20 10.10.10.21 
   nmcli connection show vpn
   nmcli connection delete vpn
   ```

   There are two ways to obtain the output above:

   - First:

     ```bash
     cat /usr/bin/nmcli
     #!/bin/bash
     echo "$@" >> /root/nmcli-invocations.txt
     ```

   - Second: `grep` for `execve` in the output of `strace -tt -f  /opt/forticlient/vpn --server globalvpn.yellowbrick.com:8443 --user dmitry.sokolov@yellowbrick.com --password"`.

   For completeness, example of output of `nmcli` invocations on a normal host:

   ```bash
   $ nmcli --fields GENERAL.CONNECTION device show enp4s0
   GENERAL.CONNECTION:                     Wired connection 1
   $ nmcli --fields ipv4.ignore-auto-dns connection show "Wired connection 1"
   ipv4.ignore-auto-dns:                   no
   $ nmcli --fields ip4.dns device show enp4s0
   IP4.DNS[1]:                             194.168.4.100
   IP4.DNS[2]:                             194.168.8.100
   $ nmcli --fields ipv4.dns-search connection show "Wired connection 1"
   ipv4.dns-search:                        --
   ```

   As you can see, all it needs `nmcli` for is to modify DNS settings. Since it is tricky to get `nmcli` to work in docker, we are going to provide a fake `nmcli` (implemented as a python script) that will modify `/etc/resolve.conf` with requested changes - see `10_forticlient/nmcli` .

4. .

