# ======================================== Header ========================================
function impl_mixin_deps
end


# ========================================================================================
function impl_omit_first_n_args --argument-names n arguments
   if test (count $argv) -ge (math $n + 2)
      for arg in $argv[(math $n + 2)..(count $argv)]
         echo $arg
      end
   end
end



function impl_set_dict_key --argument-names dict_name key arguments
   set key (string replace --all / _ $key)
   set -g __dict__{$dict_name}__{$key} (impl_omit_first_n_args 2 $argv)
end
function impl_erase_dict_key --argument-names dict_name key
   set key (string replace --all / _ $key)
   set --erase __dict__{$dict_name}__{$key}
end
function impl_get_dict_key --argument-names dict_name key
   set key (string replace --all / _ $key)
   string split ' ' --  (eval echo \$__dict__{$dict_name}__{$key})
end


function RunVerbosely
   echo -e (set_color brblack)(string escape -- $argv)(set_color normal)
   $argv
end


function impl_get_path_info --argument-names path -d 'Returns directory, basename, ext from the path'
    echo $path | sed 's/\(.*\)\/\(.*\)\.\(.*\)$/\1\n\2\n\3/'
end
function impl_get_filename --argument-names path -d 'Returns filename'
    set -l info (impl_get_path_info $path)
    echo "$info[2].$info[3]"
end
function impl_get_basename --argument-names path -d 'Returns basename'
    set -l info (impl_get_path_info $path)
    echo "$info[2]"
end
function impl_get_path --argument-names path -d 'Returns path'
    set -l info (impl_get_path_info $path)
    echo "$info[1]"
end
