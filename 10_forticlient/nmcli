#!/bin/python3

import re
import os
import sys
import logging
import typing as t
import unittest as ut
import dataclasses as dc

LOG_FILE: str = f'{os.environ["HOME"]}/nmcli.log'
sys.stderr = open(LOG_FILE, 'w') # Redirect all python interpreter errors to LOG_FILE as well
logging.basicConfig(format='%(process)d %(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                    filename=LOG_FILE)
# Exit on critical log
class ShutdownHandler(logging.Handler):
    def emit(self, record):
        print(record, file=sys.stderr)
        logging.shutdown()
        sys.exit(1)
logger = logging.getLogger()
logger.addHandler(ShutdownHandler(level=logging.CRITICAL))


@dc.dataclass
class ExecRes:
    out: str = ""
    ret: int = 0
    def is_ok(self) -> bool:
        return self.ret == 0
    def is_err(self) -> bool:
        return not self.is_ok()

def exec(dry_run: bool, command: str) -> ExecRes:
    logger.info(f'{"NOT " if dry_run else ""}executing: {command}')
    if dry_run:
        return ExecRes()

    import subprocess as sp
    completed: sp.CompletedProcess = sp.run(command, capture_output=True, shell=True)
    if completed.returncode != 0:
        logger.info(f'Failed to execute: return: {completed.returncode}, stdout: {completed.stdout}, stderr: {completed.stderr}')
    return ExecRes(out=completed.stdout, ret=completed.returncode)


# ==============================================================================================================

def remove_and_insert(file: str, remove: re.Pattern, insert: t.List[str]) -> None:
    with open(file, "r") as f:
        lines = f.readlines()
    with open(file, "w") as f:
        for line in lines:
            if remove.match(line.strip("\n")):
                continue
            f.write(line)
        for value in insert:
            f.write(value + '\n')


FAKE_CONNECTION_NAME: str = "ConnectionName1"
DRY_RUN: bool = False
RESOLV_CONF: str = "/etc/resolv.conf"

def nmcli_device(fields: t.Union[None, str], argv: t.List[str]) -> str:
    if argv[0] == "show":
        logger.info(f'Processing show request with args: {argv}, and fields: {fields}')
        # --fields GENERAL.CONNECTION device show eth0
        # --fields ip4.dns device show eth0
        if fields and fields == "GENERAL.CONNECTION":
            return FAKE_CONNECTION_NAME
        else:
            return ""
    if argv[0] == "reapply":
        logger.info(f'Processing reapply request with args: {argv}, and fields: {fields}')
        return ""
    logger.info(f'Unknown request with args: {argv}, returning empty string')
    return ""


def nmcli_connection(fields: t.Union[None, str], argv: t.List[str]) -> str:
    if argv[0] == "show":
        logger.info(f'Processing show request with args: {argv}, and fields: {fields}')
        # --fields ipv4.ignore-auto-dns connection show <DimanNe-GoogleWiFi>
        # --fields ipv4.dns-search connection show <DimanNe-GoogleWiFi>
        # connection show vpn
        return ""
    if argv[0] == "delete":
        logger.info(f'Processing delete request with args: {argv}, and fields: {fields}')
        # connection delete vpn
        return ""

    if argv[0] == "modify":
        # connection modify <DimanNe-GoogleWiFi> ipv4.ignore-auto-dns yes
        # connection modify <DimanNe-GoogleWiFi> ipv4.dns 10.10.10.20 10.10.10.21
        # connection modify <DimanNe-GoogleWiFi> ipv4.dns-search yellowbrick.io slc.yellowbrick.io
        # connection modify vpn ipv4.dns 10.10.10.20 10.10.10.21
        if len(argv) < 3:
            logger.info(f'Unexpected length of: {argv}')
            return ""
        if argv[1] == "vpn":
            logger.info(f'Ignoring non-{FAKE_CONNECTION_NAME} modify request')
            return ""

        param: str = argv[2]
        if param == "ipv4.ignore-auto-dns":
            logger.info(f'Processing modify ipv4.ignore-auto-dns request with args: {argv}')
            return ""
        if param == "ipv4.dns":
            logger.info(f'Processing modify ipv4.dns request with args: {argv}')
            values: t.List[str] = [f'nameserver {ip}' for ip in filter(None, argv[3].split(' '))]
            remove_and_insert(file=RESOLV_CONF, remove=re.compile(r'^[ \t]*nameserver[ \t]+.*'), insert=values)
            return ""
        if param == "ipv4.dns-search":
            logger.info(f'Processing modify ipv4.dns-search request with args: {argv}')
            values: t.List[str] = [f'search {suffix}' for suffix in filter(None, argv[3].split(' '))]
            remove_and_insert(file=RESOLV_CONF, remove=re.compile(r'^[ \t]*search[ \t]+.*'), insert=values)
            return ""
        logger.info(f'Processing unknown modify request with args: {argv}')
        return ""

    logger.info(f'Unknown request: {argv}, returning empty string')
    return ""

# ==============================================================================================================

# class Test(ut.TestCase):
#     def __init__(self, methodName='runTest'):
#         super().__init__(methodName)
#     def test_prev_match(self):
#         pass


# ==============================================================================================================


def main() -> None:
    logger.setLevel(level=logging.INFO)
    # ut.main(argv=['first-arg-is-ignored'], exit=False)
    argv: t.List[str] = sys.argv[1:]

    # First, let's extract the value of "--fields asdf", if present:
    fields_ind: t.Union[None, int] = next((x[0] for x in enumerate(argv) if x[1] == '--fields'), None)
    fields: t.Union[None, str] = None
    if fields_ind is not None:
        fields = argv[fields_ind + 1]
        del argv[fields_ind:fields_ind + 2]

    # Find indices of all supported commands (device & connection for now) and execute the one that has the least index:
    device_ind: t.Union[None, int] = next((x[0] for x in enumerate(argv) if x[1] == 'device'), None)
    connection_ind: t.Union[None, int] = next((x[0] for x in enumerate(argv) if x[1] == 'connection'), None)
    cmds: t.List[t.Union[None, int]] = [device_ind, connection_ind]

    result: str = ""
    if all(v is None for v in cmds):
        result = ""
        logger.info(f'Got request: {argv}, fields: {fields}: Unknown command')
    else:
        cmd_ind_to_exec: t.List[t.Union[None, int]] = sorted(cmds, key=lambda x: (x is None, x))
        if argv[cmd_ind_to_exec[0]] == "device":
            logger.info(f'Got request "device" command with args: {argv}, and fields: {fields}')
            del argv[cmd_ind_to_exec[0]]
            result = nmcli_device(fields, argv)
        elif argv[cmd_ind_to_exec[0]] == "connection":
            logger.info(f'Got "connection" command with args: {argv}, and fields: {fields}')
            del argv[cmd_ind_to_exec[0]]
            result = nmcli_connection(fields, argv)

    logger.info(f'Response: "{result}"')
    if result:
        print(result)


main()
