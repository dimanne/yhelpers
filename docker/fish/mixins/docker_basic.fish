# ======================================== Header ========================================
function impl_mixin_deps
end
# ========================================================================================


# ========================================================================================
function att --argument-names name uid command
   if test -n "$uid"
      set u_arg -u $uid
      if test $uid -eq 0
         # Add --privileged: https://unix.stackexchange.com/questions/136690/how-can-i-substitute-lsof-inside-a-docker-native-not-lxc-based
         set u_arg $u_arg --privileged
      end
   end

   set commands /bin/fish /bin/bash
   if test -n "$command"
      set -p commands $command
   end

   for cmd in $commands;
      echo "Trying $cmd..."
      docker exec -it $u_arg $name $cmd
      if test $status -eq 0
         break
      end
   end
end

function att_latest --argument-names uid
   set name (docker ps -l --format '{{ .Names }}')
   att $name $uid
end

# Completion for att, taken from http://fishshell.com/docs/current/index.html#completion-own
complete -c att -f
complete -c att -a "(docker ps --format '{{ .Names }}')"



function impl_docker_hidpi
   # https://wiki.archlinux.org/title/HiDPI#GUI_toolkits
   string split ' ' --  --env QT_FONT_DPI=140
   string split ' ' --  --env GDK_SCALE=2
   string split ' ' --  --env GDK_DPI_SCALE=0.7
end

function impl_docker_term
   string split ' ' --  --env TERM --env COLORTERM
end

function impl_docker_x11 --argument-names home
   string split ' ' --  --env DISPLAY
   string split ' ' --  --volume /tmp/.X11-unix:/tmp/.X11-unix
   string split ' ' --  --volume $HOME/.Xauthority:$home/.Xauthority:ro
end

function impl_docker_wayland
end

function impl_docker_make_and_print  --argument-names src dst
   mkdir -p                      $src
   string split ' ' --  --volume $src:$dst
end

function impl_fish_run_time_mounts --argument-names host_home # host_home == $HOME/docker_mounts/yellow_vpn/
   impl_docker_make_and_print $host_home/.local/share/fish $HOME/.local/share/fish
end

function impl_docker_generate_name --argument-names hint
   # Given "typora" as a hint, the functions will generate:
   # dimanne_typora
   # then, dimanne_typora_2
   # then, dimanne_typora_3
   # etc...

   if test -z "$hint"
      # No hint => no name_arg => let docker choose it for us
      return
   end
   set hint (whoami)_$hint
   set names (docker ps --format '{{ .Names }}')
   # echo "hint: $hint, names: $names"
   # string split ' ' $names | grep "^$hint\$"
   set largest (string split ' ' $names | grep -v "^$hint\$" | grep -P "^$hint_\d+\$" | sed "s/$hint_//g" | sort -n | tail -n 1)
   if test -n "$largest"
      # echo "largest: $largest"
      echo {$hint}_(math $largest + 1)
      return
   end
   # largest is empty
   if string split ' ' $names | grep "^$hint\$" 1>/dev/null
      echo {$hint}_2
      return
   end
   echo $hint
end

function impl_docker_generate_name_arg_and_host_arg --argument-names hint
   set -l name (impl_docker_generate_name $hint)
   string split ' ' -- --name $name
   string split ' ' -- --hostname $name
end

# cat /etc/nvidia-container-runtime/config.toml
# no-cgroups = false # -> true
# impl_docker_rm_nvidia_header
function impl_docker_nvidia_args
   string split ' ' -- --runtime nvidia --device /dev/nvidia0 --device /dev/nvidia1 --device /dev/nvidiactl --device /dev/nvidia-modeset --device /dev/nvidia-uvm
end
function impl_docker_rm_header
   string split ' ' -- -it --shm-size 3G --rm --user (id -u)
end
