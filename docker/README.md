# How to use


# Implementation details

## The Mixins concept

Each of the directories here has a small, **well-scoped** **mixin** Dockerfile with a single responsibility.

For example:

* `10_home_generic/Dockerfile` sets up OS user (username, uid, gid, home).
* `20_ides_pycharm` installs PyCharm.
* etc...

The most important thing across all Dockerfiles here is that they start with:
```
ARG base_image
FROM $base_image
```
it allows us to chain them together in `build.py`

## build.py

This is where small mixin Dockerfiles are chained together to create a unified image.

## Images configuration

This is just a python dict:

* **Keys** are names for semantically grouped mixin Dockerfiles (for example, `"dimanne/impedance"` means that it is my image for host/computer "impedance").
* **Values** are list of mixin Dockerfiles.


![](Screenshot_20211007_164431.png)


Now I can start docker image "dimanne/dev_yellow".

## Helper scripts
