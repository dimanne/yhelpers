#!/bin/bash

# Usage:
# Create a file credentials.sh in this dir with the following content:
# FORTI_PASS=".."
# FORTI_SERVER_PORT="..."
# FORTI_USERNAME="..."
#
# And then:
# /vpn/forti_start.sh

THIS_DIR=$(cd "$(dirname "$0")"; pwd)
source $THIS_DIR/credentials.sh

# function cleanup {
#    echo "Cleaning up routes..."
#    ip route del default via 192.168.86.1
#    ip route del 169.254.0.0/16 dev wlp0s20f3
#    ip route del 192.168.86.0/24 dev wlp0s20f3
#    dhclient -r wlp0s20f3; sudo dhclient wlp0s20f3
#    echo "Finished cleaning up routes"
# }
# 
# cleanup
# echo "Starting FortiClient..."
# $THIS_DIR/forti_start.exp $1 $2 -d
# echo "FortiClient stopped"
# cleanup


# Restore /etc/resolv.conf
cat << EOF > /etc/resolv.conf
# We have to have this resolv.conf explicitly, because VPN is going to change it
nameserver 8.8.8.8
EOF


/vpn/NetworkManager&
export FORTI_PASS=$FORTI_PASS
$THIS_DIR/forti_start.exp $FORTI_SERVER_PORT $FORTI_USERNAME -d
