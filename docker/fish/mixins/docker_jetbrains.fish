# ======================================== Header ========================================
function impl_mixin_deps
   set this_dir (dirname (realpath (status current-filename)))
   echo $this_dir/docker_basic.fish
end
# ========================================================================================



function impl_docker_jetbrains_mounts
   impl_docker_make_and_print $HOME/docker_mounts/JetBrains/.cache/JetBrains       $HOME/.cache/JetBrains:rw
   impl_docker_make_and_print $HOME/docker_mounts/JetBrains/.config/JetBrains      $HOME/.config/JetBrains:rw
   impl_docker_make_and_print $HOME/docker_mounts/JetBrains/.java                  $HOME/.java:rw
   impl_docker_make_and_print $HOME/docker_mounts/JetBrains/.local/share/JetBrains $HOME/.local/share/JetBrains:rw
end


function impl_dev_pycharm --argument-names image name_suffix arguments
   docker run (impl_docker_rm_header) (impl_docker_term) (impl_docker_x11 $HOME) \
              (impl_docker_generate_name_arg_and_host_arg "pycharm$name_suffix") \
              (impl_docker_jetbrains_mounts) \
              --volume $HOME/devel:$HOME/devel:rw \
              $argv[3..-1] \
              --cap-add NET_ADMIN \
              $image /IDEs/pycharm-community/bin/pycharm.sh
end
