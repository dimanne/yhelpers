# ======================================== Header ========================================
function impl_mixin_deps
   set this_dir (dirname (realpath (status current-filename)))
   echo $this_dir/mixins/common_fish_setup.fish
   echo $this_dir/mixins/bat.fish
end
function impl_config_fish
end
function impl_fish_home_path
   echo $HOME # Run only from within docker, hence $HOME
end



# =======================================================================================
