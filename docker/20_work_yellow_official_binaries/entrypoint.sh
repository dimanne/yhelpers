#!/bin/bash

me=`basename "$0"`
# echo "$me: Adding internal hosts to /etc/hosts..."
# cat /etc/hosts.txt | sudo tee -a /etc/hosts

# echo "$me: Setting up routes..."
# if test -n "$YELLOW_VPN_CONTAINER_IP"; then
#    sudo ip route add 10.10.52.0/24 via $YELLOW_VPN_CONTAINER_IP
# fi

echo "$me: Updating env vars..."
export PATH=/usr/local/ybd-mgmt-install/toolchain/clang-4.0.0/bin/:$PATH
export YBPASSWORD=ybd

echo "$me: Sourcing source_ybd_variables.sh..."
source $HOME/devel/ytest/utils/source_ybd_variables.sh
env | grep "YBD_"
cd $HOME/devel/ytest/
echo "$me: Example of usage:"
echo "  * ytest run -h \$YELLOW_CONTAINER_IP -p 30 lists/checkin.list"
echo "  * ytest run -h \$YELLOW_CONTAINER_IP -p 30 Tests/sql/pesticide_queries"
echo "  * ytest run -h \$YELLOW_CONTAINER_IP -p 30 --recurse \$(find ./Tests/sql -mindepth 3 -maxdepth 3 -type d | grep -v 'Tests/sql/functions/string/encode\|Tests/sql/functions/string/to_char\|Tests/sql/planner/inlist/boolean_to_inlist') # To exclude some test"
echo "  * cd $HOME/devel/engeff_tools/ytestperf/src; ./ytestperf -h \$YELLOW_CONTAINER_IP -d yellowbrick_test -g -ds -t TPCDS_SF1 --disable-hw --disable-ssh"


# This will exec the CMD from your Dockerfile, i.e. "npm start"
exec "$@"
