### Patch the repo:
```
git diff
diff --git a/conanfile.py b/conanfile.py
index 0ddcf796..9df9bf5d 100644
--- a/conanfile.py
+++ b/conanfile.py
@@ -24,7 +24,7 @@ class OrbitConan(ConanFile):
     settings = "os", "compiler", "build_type", "arch"
     generators = ["cmake_multi"]
     options = {"system_mesa": [True, False],
-               "system_qt": [True, False], "with_gui": [True, False],
+               "system_qt": [True, False], "with_gui": [False, False],
                "debian_packaging": [True, False],
                "fPIC": [True, False],
                "crashdump_server": "ANY",
@@ -34,7 +34,7 @@ class OrbitConan(ConanFile):
                "build_target": "ANY",
                "deploy_opengl_software_renderer": [True, False]}
     default_options = {"system_mesa": True,
-                       "system_qt": True, "with_gui": True,
+                       "system_qt": True, "with_gui": False,
                        "debian_packaging": False,
                        "fPIC": True,
                        "crashdump_server": "",
diff --git a/src/LinuxTracing/PerfEventOpen.cpp b/src/LinuxTracing/PerfEventOpen.cpp
index 94d3be1e..6ee995da 100644
--- a/src/LinuxTracing/PerfEventOpen.cpp
+++ b/src/LinuxTracing/PerfEventOpen.cpp
@@ -91,7 +91,7 @@ int callchain_sample_event_open(uint64_t period_ns, pid_t pid, int32_t cpu,
   pe.sample_period = period_ns;
   pe.sample_type |= PERF_SAMPLE_CALLCHAIN;
   // TODO(kuebler): Read this from /proc/sys/kernel/perf_event_max_stack
-  pe.sample_max_stack = 127;
+  // pe.sample_max_stack = 127;
   pe.exclude_callchain_kernel = true;
   // Exclude all samples that fall into the kernel. In particular this will discard samples falling
   // into the int3 triggered uprobe code, which we could otherwise not really detect.
```

### Build it in bare ubuntu:16.04 docker:
```
docker run -it --rm (impl_docker_term) --volume $HOME/devel:$HOME/devel:rw ubuntu:16.04 /bin/bash

apt update && apt install -y software-properties-common && add-apt-repository --yes ppa:ubuntu-toolchain-r/test && add-apt-repository --yes ppa:deadsnakes/ppa && apt update && apt install -y git clang-8 cmake gcc-9 g++-9 python3 python3-pip sudo lsb-release python3.6 apt-transport-https wget curl

wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null && apt-add-repository "deb https://apt.kitware.com/ubuntu/ $(lsb_release -cs) main" && apt update && apt install -y cmake

update-alternatives --install /usr/bin/python3 python /usr/bin/python3.6 2 && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 2 && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 2


cd /home/dimanne/ && mkdir -p tmp && cp -r devel/orbit ./tmp/ && cd tmp/orbit/ && git clean -dfx
export PATH=$HOME/.local/bin:$PATH && ./bootstrap-orbit.sh --ignore-system-requirements --dont-compile
./build.sh clang8_relwithdebinfo
```

### Take results:
```
docker cp condescending_hopper:/home/dimanne/tmp/orbit/build_clang8_relwithdebinfo/bin/OrbitService ~/devel/
docker cp condescending_hopper:/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.28 ~/devel/libstdc++.so.6

scp $HOME/devel/OrbitService $HOME/devel/libstdc++.so.6 ybdadmin@y2b08.slc.yellowbrick.io:

scp ./OrbitService ./libstdc++.so.6 root@192.168.10.10:

ssh root@192.168.10.10
chmod 777 ./libstdc++.so.6
export LD_LIBRARY_PATH=/root:/ybd

# ./OrbitService 
[2022-01-13T19:06:01.460575] [             Service/OrbitService.cpp:36] uname -a: Linux ybos-00000000-0000-0000-0000-38B8EBD2048C 5.4.69 #2 SMP Fri Jan 7 15:30:10 UTC 2022 x86_64 GNU/Linux
[2022-01-13T19:06:01.460634] [             Service/OrbitService.cpp:48] Error: Unable to open file "/usr/local/cloudcast/VERSION": No such file or directory
[2022-01-13T19:06:01.460641] [             Service/OrbitService.cpp:58] Error: Unable to open file "/usr/local/cloudcast/BASE_VERSION": No such file or directory
[2022-01-13T19:06:01.460648] [             Service/OrbitService.cpp:68] Error: Unable to open file "/usr/local/cloudcast/INSTANCE_VERSION": No such file or directory
sh: /usr/local/cloudcast/bin/gpuinfo: No such file or directory
[2022-01-13T19:06:01.461661] [             Service/OrbitService.cpp:82] Error: Could not execute "/usr/local/cloudcast/bin/gpuinfo driver-version"
[2022-01-13T19:06:01.461669] [            Service/OrbitService.cpp:146] Running Orbit Service version 1.70-89-g962c575
[2022-01-13T19:06:01.461678] [            Service/OrbitService.cpp:110] Starting gRPC server at 127.0.0.1:44765
[2022-01-13T19:06:01.462696] [    CaptureService/CaptureService.cpp:38] Clock resolution: 30 (ns)
[2022-01-13T19:06:01.463668] [            Service/OrbitService.cpp:116] gRPC server is running
[2022-01-13T19:06:01.463691] [            Service/OrbitService.cpp:134] Starting producer-side server at /tmp/orbit-producer-side-socket
[2022-01-13T19:06:01.463797] [            Service/OrbitService.cpp:140] Producer-side server is running
```

### Tunnel ports:


Terminal 1 (run the server on a worker):
```
ssh -tt ybdadmin@y2b08.slc.yellowbrick.io ssh root@192.168.10.10
export LD_LIBRARY_PATH=/root:/ybd && ./OrbitService
```

Terminal 2 (44765 port of worker -> manager node):
```
ssh ybdadmin@y2b08.slc.yellowbrick.io
ssh -L 44765:127.0.0.1:44765 root@192.168.10.10
```

Terminal 3 (44765 port of manager node -> local docker interface):
```
ssh -L 172.17.0.1:44765:127.0.0.1:44765 ybdadmin@y2b08.slc.yellowbrick.io
```

Terminal 4 (44765 port of docker interface -> specific container):
```
att dimanne_qtcreator_yellow
socat tcp-listen:44765,reuseaddr,fork tcp:172.17.0.1:44765
```

Terminal 5 (GUI)
```
att dimanne_qtcreator_yellow
$HOME/devel/orbit/build_clang12_relwithdebinfo/bin/Orbit --local
```
