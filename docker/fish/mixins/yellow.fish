# ======================================== Header ========================================
function impl_mixin_deps
end
# ========================================================================================




function impl_init_and_start_vpn --argument-names config_path
   if not command --quiet -v openfortivpn
      echo "openfortivpn is not installed => installing it..."
      sudo apt update && sudo apt install openfortivpn
   end
   if not test -e $config_path
      echo "there is no openfortivpn config => setting it up..."
      read -P "Your <login>@yellowbrick.com (without @yellowbrick.com): " login
      read -sP "Your password: " pass
      cp /usr/share/openfortivpn/config.template $config_path
      sed -i "s/^ *host *= *.*/host = globalvpn.yellowbrick.com/g" $config_path
      sed -i "s/^ *port *= *.*/port = 8443/g" $config_path
      sed -i "s/^ *username *= *.*/username = $login@yellowbrick.com/g" $config_path
      sed -i "s/^ *password *= *.*/password = $pass/g" $config_path
      echo "persistent = 10800" >> $config_path
   end
   if pgrep -a -f "openfortivpn "
      echo "openfortivpn is already running, kill it with 'sudo pkill -f openfortivpn' first. Exiting..."
      return
   end
   set -l fish_trace on
   sudo openfortivpn -c $config_path
end


function dev_yellow_vpn
   impl_init_and_start_vpn ~/forti.conf
end


function impl_get_vpn_dnses
   string split ' ' -- (cat /etc/resolv.conf | grep -P "^ *nameserver +.*" | grep -v "nameserver 127\." | tr -s ' ' | cut -d ' ' -f 2 | xargs -I {} echo "--dns {}")
   string split ' ' -- "--dns 8.8.8.8"
end

function impl_yellow_testenv --argument-names image args
   dev_yellow_vpn
   docker run (impl_docker_rm_header) (impl_docker_term) \
      (impl_docker_generate_name_arg_and_host_arg yellow_official) \
      --env YELLOW_CONTAINER_IP=(docker container inspect  -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dimanne_qtcreator_yellow) \
      --cap-add NET_ADMIN \
      --volume $HOME/devel:$HOME/devel:rw \
      --volume $HOME/devel/qumulo/data:/data:rw \
      (impl_get_vpn_dnses) \
      $argv[2..-1] \
      $image /entrypoint.sh /bin/bash
end
