# ======================================== Header ========================================
function impl_mixin_deps
   set this_dir (dirname (realpath (status current-filename)))
   echo $this_dir/docker_basic.fish
end
# ========================================================================================



function impl_docker_qtcreator_mounts
   if not test -e $HOME/docker_mounts/QtCreator/.config/QtProject/
      mkdir -p $HOME/docker_mounts/QtCreator/.config/QtProject
      cp -r $HOME/devel/scripts/IDE_settings/QtCreator-4.8/.config/QtProject $HOME/docker_mounts/QtCreator/.config/
   end
   impl_docker_make_and_print $HOME/docker_mounts/QtCreator/.config/QtProject                               $HOME/.config/QtProject
   impl_docker_make_and_print $HOME/docker_mounts/QtCreator/.local/share/org.kde.syntax-highlighting/syntax $HOME/.local/share/org.kde.syntax-highlighting/syntax
end


function impl_dev_qtc --argument-names image name_suffix arguments
   docker run (impl_docker_rm_header) (impl_docker_term) (impl_docker_x11 $HOME) \
              (impl_docker_generate_name_arg_and_host_arg "qtcreator$name_suffix") \
              (impl_docker_qtcreator_mounts) \
              --volume $HOME/devel:$HOME/devel:rw \
              $argv[3..-1] \
              --cap-add SYS_ADMIN  --cap-add SYS_PTRACE --cap-add NET_ADMIN \
              $image /bin/bash -c 'while :; do /IDEs/qtcreator/bin/qtcreator.sh; done'
end
