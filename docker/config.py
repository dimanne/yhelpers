import os
import pwd
import build
import typing as t
import pathlib as pl

repo_docker = pl.Path(__file__).expanduser().resolve().parent
username: str = pwd.getpwuid(os.getuid())[0] # or getpass.getuser()

def _00_CONT_GENERIC_HOME_TUPLE(uid: int = os.geteuid(), gid: int = os.getgid(), uname: str = username) -> build.Dockerfile:
    return build.Dockerfile(repo_docker / "00_generic_home",
                      args={"user_id": uid, "user_group": gid, "user_name": uname},
                      name_transformations=[lambda x: f'{x}-{uname}'])


configs: t.Dict[str, build.Dockerfiles] = {
    # ================================================================================================================
    # Any user, any device: Common across users and devices

    "user_utils_sudo": build.Dockerfiles(is_root_need_to_tag=False, list=[
        build.Dockerfile(repo_docker / "00_ubuntu"),
        _00_CONT_GENERIC_HOME_TUPLE(),
        build.Dockerfile(repo_docker / "00_sysutils"),
        build.Dockerfile(repo_docker / "10_sudo"),
        build.Dockerfile(repo_docker / "10_user_home_setup"),
        build.Dockerfile(repo_docker / "20_fish", args={"fish_install_script_args": "env_dockcont_dev_base.fish"}),
    ]),

    "yellow/dev_gui_less": build.Dockerfiles(is_root_need_to_tag=True, list=[
        build.Dockerfile("ubuntu:21.04"),
        build.Dockerfile("user_utils_sudo"),
        build.Dockerfile(repo_docker / "00_base_cpp"),
        build.Dockerfile(repo_docker / "00_clang12"),
        build.Dockerfile(repo_docker / "00_base_python"),
        build.Dockerfile(repo_docker / "20_work_yellow"),
    ]),

    "yellow/dev_ides_gl_less": build.Dockerfiles(is_root_need_to_tag=True, list=[
        build.Dockerfile("yellow/dev_gui_less"),
        build.Dockerfile(repo_docker / "00_qt_gui_apps_libs"),
        build.Dockerfile(repo_docker / "20_ides_qtcreator"),
        build.Dockerfile(repo_docker / "20_ides_pycharm"),
    ]),

    "yellow/dev_testenv": build.Dockerfiles(is_root_need_to_tag=True, list=[
        # Pull the images via docker pull docker-dev.yellowbrick.io/build-ybd-stack-ubuntu-16.04:v5 && docker pull docker-dev.yellowbrick.io/ybd-test-toolchain:0.1.0-20210607173144-18
        build.Dockerfile("docker-dev.yellowbrick.io/ybd-test-toolchain:0.1.0-20210607173144-18"),
        build.Dockerfile("user_utils_sudo"),
        build.Dockerfile(repo_docker / "00_base_cpp"),
        build.Dockerfile(repo_docker / "20_work_yellow"),
        build.Dockerfile(repo_docker / "20_work_yellow_official_binaries"),
    ]),

    # ================================================================================================================
    # Specific user, various devices's umbrellas:

    "yellow/laptop": build.Dockerfiles(is_root_need_to_tag=False, list=[
        build.Dockerfile("yellow/dev_ides_gl_less"),
        build.Dockerfile("yellow/dev_testenv"),
    ]),
}



# ================================================================================================================
# Main build.py interface:

class ConfigProvider(build.ConfigProvider):
    def configs(self) -> t.Dict[str, build.Dockerfiles]:
        return configs

    def image_prefix(self):
        return ""
