#!/usr/bin/python3

import os
import abc
import sys
import logging
import argparse
import typing as t
import pathlib as pl
import dataclasses as dc


logging.basicConfig(format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s')

# Exit on critical log
class ShutdownHandler(logging.Handler):
    def emit(self, record):
        print(record, file=sys.stderr)
        logging.shutdown()
        sys.exit(1)
logger = logging.getLogger()
logger.addHandler(ShutdownHandler(level=logging.CRITICAL))


class c:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# ================================================================================================================
# ================================================================================================================
# Datatypes for describing data:

@dc.dataclass
class Dockerfile:
    dir_or_group_name: t.Union[pl.Path, str] = "" # both: name of directory or group of images in configs array
    args: t.Dict[str,t. Any] = dc.field(default_factory=dict)
    name_transformations: t.List[t.Callable[[str], str]] = dc.field(default_factory=list)

    def get_image_name(self) -> str:
        assert(self.is_dir())
        result: str = self.dir_or_group_name.name
        for transformation in self.name_transformations:
            result = transformation(result)
        return result
    def is_dir(self) -> bool:
        return isinstance(self.dir_or_group_name, pl.Path)
    def is_group_name(self) -> bool:
        return isinstance(self.dir_or_group_name, str)
    def as_dir(self) -> pl.Path:
        assert(self.is_dir())
        return self.dir_or_group_name
    def as_group_name(self) -> str:
        assert(self.is_group_name())
        return self.dir_or_group_name


@dc.dataclass
class Dockerfiles:
    list: t.List[Dockerfile] = dc.field(default_factory=list)
    is_root_need_to_tag: bool = dc.field(default_factory=bool)


class ConfigProvider(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def configs(self) -> t.Dict[str, Dockerfiles]:
        pass
    @abc.abstractmethod
    def image_prefix(self):
        return ""


# ================================================================================================================
# ================================================================================================================
# Processing:

def python_import_exists(import_name: str) -> bool:
    from importlib import util
    return util.find_spec(import_name) is not None


class Builder:
    def __init__(self, global_prefix: str, dry_run):
        self.built_images_names = set()
        self.dry_run = dry_run
        self.global_prefix = global_prefix

    def _build_one(self, base_image_name: str, this_image_name: str, build_args: t.Dict[str, str], docker_file_dir: str):
        logger.info(f'Building image {this_image_name} from Dockerfile at: {docker_file_dir} with args: {build_args}')

        os.environ["DOCKER_BUILDKIT"] = "1"
        s = " --build-arg "
        str_build_args = f'{s}{s.join([f"{n}={v}" for n, v in build_args.items()])}'.strip()
        external_opts = ' '.join(self.docker_build_opts).strip()
        docker_build_command = f'docker build --rm=true -t {this_image_name} {str_build_args} {docker_file_dir} {external_opts}'

        # Example:
        # Command: docker build --rm=true -t dimanne/10_home_generic
        #                       --build-arg user_id=1000
        #                       --build-arg user_group=1000
        #                       --build-arg user_name=dimanne
        #                       --build-arg base_image=dimanne/00_ubuntu
        #                       /home/dimanne/devel/scripts/docker/10_home_generic
        logger.info(f'Command: {c.BOLD}{docker_build_command}{c.ENDC}')

        if self.dry_run:
            return

        # Curses try #2: https://stackoverflow.com/questions/24946988/using-python-subprocess-call-to-launch-an-ncurses-process

        if not python_import_exists("pexpect"):
            logger.critical(f"{c.BOLD}{c.FAIL}Python module pexpect is not installed, Install it manually: "
                            f"sudo apt install python3-pexpect{c.ENDC}. Exiting...")
        import pexpect
        child = pexpect.spawn(docker_build_command)
        child.interact()
        child.close()
        if child.exitstatus != 0:
            exit(1)

    def _tag(self, name: str, image_to_tag: str):
        # If global_prefix not set, or name already starts with it - use name, otherwise, prefix it with global_prefix:
        tag_name: str = name if not self.global_prefix or name.startswith(self.global_prefix + '/') else f'{self.global_prefix}/{name}'
        logger.info(f'Tagging last built image {image_to_tag} with tag: {tag_name}')
        if not self.dry_run:
            if not python_import_exists("docker"):
                logger.critical(f"{c.BOLD}{c.FAIL}Python module docker is not installed, Install it manually: "
                                f"sudo apt install python3-docker{c.ENDC}. Exiting...")
            import docker
            with docker.APIClient(base_url='unix://var/run/docker.sock') as client:
                client.tag(image=image_to_tag, repository=tag_name, tag='latest', force=True)

    def build(self, configs: t.Dict[str, Dockerfiles], name: str, docker_build_opts: t.List[str] = []) -> None:
        import copy
        self.docker_build_opts = docker_build_opts
        for n, dockerfiles in configs.items():
            dockerfiles.list = copy.deepcopy(dockerfiles.list)
            if self.global_prefix:
                for dockerfile in dockerfiles.list:
                    dockerfile.name_transformations.append(lambda x: f'{self.global_prefix}/{x}')
        self._build(configs, name, "", False)

    def _build(self, configs: t.Dict[str, Dockerfiles], name: str, last_built_image_name: str, base_on_each_other: bool) -> str:
        logger.info(f'Recursing to build Dockerfiles: {name}')
        if name not in configs or len(configs[name].list) == 0:
            logger.critical(f'{c.BOLD}{c.FAIL}There is no {name} in configs or list of dockerfiles is empty{c.ENDC}. Exiting...')
        group: Dockerfiles = configs[name]

        if not (group.is_root_need_to_tag or base_on_each_other):
            # Do not consider the group as one single entity, just process them one by one as a list of independent items
            for item in group.list:
                self._build(configs, item.as_group_name(), "", False)
            return ""

        for child in group.list:
            # 1. child.dir_or_group_name is a sub-config group:
            if child.is_group_name() and child.as_group_name() in configs:
                # child.dir_or_group_name is a reference to sub-config (nested group), recurse:
                last_built_image_name = self._build(configs, child.as_group_name(), last_built_image_name, True)

            # Now we know that child.dir_or_group_name is either (1) a path or
            #                                                    (2) it is a group, but there is no such group in our configs

            # 2. This is a group, but it does not exists in our configs:
            elif child.is_group_name():
                # It can only happen if it is root (the first container in the chain),
                # such as ubuntu:20.04 => no need to build anything
                if last_built_image_name:
                    logger.critical(f'{c.BOLD}{c.FAIL}Dockerfile(dir_or_group_name={child.dir_or_group_name}) is (1) a name of a group '
                                    f'and (2) there is no such group in the config => interpreting it as a root image, but we have already '
                                    f'built an image: {last_built_image_name}. Did you mean to make the child '
                                    f'a directory with Dockerfile (if yes, enclose it with pl.Path)?{c.ENDC}. Exiting...')
                last_built_image_name = child.as_group_name()
                continue

            # 3. child.dir_or_group_name is path (containing a Dockerfile), build it:
            else:
                build_args: t.Dict[str,t. Any] = child.args
                build_args["base_image"] = last_built_image_name
                self._build_one(last_built_image_name,
                                child.get_image_name(),
                                build_args,
                                str(child.as_dir()))
                last_built_image_name = child.get_image_name()

        if group.is_root_need_to_tag:
            self._tag(name, last_built_image_name)

        logger.info(f'Built: {name}, last built image: {last_built_image_name}\n')
        return last_built_image_name



# ================================================================================================================
# ================================================================================================================
# Tests:

import unittest as ut
import unittest.mock as mock

class TestBuilder(ut.TestCase):
    def test_Dockerfile_str_is_group_name(self):
        df = Dockerfile("asdf")
        self.assertTrue(df.is_group_name())
        self.assertFalse(df.is_dir())

    def test_Dockerfile_path_is_dir(self):
        df = Dockerfile(pl.Path("asdf"))
        self.assertTrue(df.is_dir())
        self.assertFalse(df.is_group_name())

    def test_non_recursive_group(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2")), Dockerfile(pl.Path("/a/b/g1.3"))],
                              is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g1")
        self.assertEqual(len(builder._build_one.call_args_list), 2)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], (f"username/g1.2", f"username/g1.3"))
        self.assertEqual(len(builder._tag.call_args_list), 1)

    def test_non_recursive_group_tag_behaviour_only(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2")), Dockerfile(pl.Path("/a/b/g1.3"))],
                              is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g1")
        self.assertEqual(len(builder._tag.call_args_list), 1)
        self.assertEqual(builder._tag.call_args_list[0][0][0:2], ("g1", f"username/g1.3"))

    def test_one_recursive_group(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2")), Dockerfile(pl.Path("/a/b/g1.3"))], is_root_need_to_tag=False),
            "g2": Dockerfiles([Dockerfile("g1")], is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g2")
        self.assertEqual(len(builder._build_one.call_args_list), 2)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], (f"username/g1.2", f"username/g1.3"))
        self.assertEqual(len(builder._tag.call_args_list), 1)

    def test_one_recursive_group_and_one_elem(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2")), Dockerfile(pl.Path("/a/b/g1.3"))], is_root_need_to_tag=False),
            "g2": Dockerfiles([Dockerfile("g1"), Dockerfile(pl.Path("/a/b/g2.1"))], is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g2")
        # print(builder._build_one.call_args_list)
        self.assertEqual(len(builder._build_one.call_args_list), 3)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], (f"username/g1.2", f"username/g1.3"))
        self.assertEqual(builder._build_one.call_args_list[2][0][0:2], (f"username/g1.3", f"username/g2.1"))
        self.assertEqual(len(builder._tag.call_args_list), 1)

    def test_two_recursive_groups(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2"))], is_root_need_to_tag=True),
            "g2": Dockerfiles([Dockerfile("g2.1"), Dockerfile(pl.Path("/a/b/g2.2"))], is_root_need_to_tag=True),
            "g3": Dockerfiles([Dockerfile("g1"), Dockerfile("g2")], is_root_need_to_tag=False),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g3")
        self.assertEqual(len(builder._build_one.call_args_list), 2)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], ("g2.1", f"username/g2.2"))
        self.assertEqual(len(builder._tag.call_args_list), 2)
        self.assertEqual(builder._tag.call_args_list[0][0][0:2], (f"g1", f"username/g1.2"))
        self.assertEqual(builder._tag.call_args_list[1][0][0:2], (f"g2", f"username/g2.2"))

    def test_custom_name_transformations(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1", name_transformations=[lambda x: f'{x}-asdf']),
                               Dockerfile(pl.Path("/a/b/g1.2"), name_transformations=[lambda x: f'{x}-qwe', lambda x: f'{x}-xxx']),
                               Dockerfile(pl.Path("/a/b/g1.3"), name_transformations=[lambda x: f'{x}-zxcv'])],
                              is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g1")
        # print(builder._build_one.call_args_list)
        self.assertEqual(len(builder._build_one.call_args_list), 2)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2-qwe-xxx"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], (f"username/g1.2-qwe-xxx", f"username/g1.3-zxcv"))
        self.assertTrue(builder._build_one.call_args_list[0][0][3].endswith("/g1.2"))
        self.assertTrue(builder._build_one.call_args_list[1][0][3].endswith("/g1.3"))

    def test_nested_recursive_groups(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2"))], is_root_need_to_tag=False),
            "g2": Dockerfiles([Dockerfile("g1"), Dockerfile(pl.Path("/a/b/g2.1"))], is_root_need_to_tag=False),
            "g3": Dockerfiles([Dockerfile("g2"), Dockerfile(pl.Path("/a/b/g3.1"))], is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g3")
        # print(builder._build_one.call_args_list)
        self.assertEqual(len(builder._build_one.call_args_list), 3)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], (f"username/g1.2", f"username/g2.1"))
        self.assertEqual(builder._build_one.call_args_list[2][0][0:2], (f"username/g2.1", f"username/g3.1"))

    def test_same_dockerfile_used_in_multiple_places_do_not_append_transformation_multiple_times(self):
        df: Dockerfile = Dockerfile(pl.Path("/a/b/n"))
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), df], is_root_need_to_tag=True),
            "g2": Dockerfiles([Dockerfile("g1"), df], is_root_need_to_tag=True),
            "g3": Dockerfiles([Dockerfile("g2"), df], is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g1")
        # print(builder._build_one.call_args_list)
        self.assertEqual(len(builder._build_one.call_args_list), 1)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/n"))

    def test_the_first_container_in_the_second_group_is_based_on_the_last_container_in_the_first_group_if_is_root_is_true(self):
        configs: t.Dict[str, Dockerfiles] = {
            "g1": Dockerfiles([Dockerfile("g1.1"), Dockerfile(pl.Path("/a/b/g1.2"))], is_root_need_to_tag=False),
            "g2": Dockerfiles([Dockerfile(pl.Path("/a/b/g2.1")), Dockerfile(pl.Path("/a/b/g2.2"))], is_root_need_to_tag=False),
            "g3": Dockerfiles([Dockerfile("g1"), Dockerfile("g2")], is_root_need_to_tag=True),
        }
        builder: Builder = Builder(global_prefix="username", dry_run=True)
        builder._build_one = mock.MagicMock()
        builder._tag = mock.MagicMock()
        builder.build(configs, "g3")
        # print(builder._build_one.call_args_list)
        self.assertEqual(builder._build_one.call_args_list[0][0][0:2], ("g1.1", f"username/g1.2"))
        self.assertEqual(builder._build_one.call_args_list[1][0][0:2], (f"username/g1.2", f"username/g2.1"))
        self.assertEqual(builder._build_one.call_args_list[2][0][0:2], (f"username/g2.1", f"username/g2.2"))



# ================================================================================================================
# ================================================================================================================
# main() app

def get_usage_help(configs: t.Optional[t.Dict[str, Dockerfiles]], config_path: t.Optional[str]) -> str:
    if not configs:
        return f'Usage example: "{sys.argv[0]} --config ~/my/config.py image/name123"'

    help: str = "Usage:\n"
    for config_name, _ in configs.items():
        help += f'   {sys.argv[0]} --config {config_path} {config_name}\n'
    return help


def print_usage_and_exit(configs: t.Optional[t.Dict[str, Dockerfiles]], config_path: t.Union[str, None]):
    print(get_usage_help(configs, config_path))
    exit(1)


def main():
    parser = argparse.ArgumentParser(description=f'Script that takes config, group id, and builds dockerfiles.'
                                                 f'{get_usage_help(None, None)}')
    parser.add_argument("--dry-run", action='store_true')
    parser.add_argument("--log-level", type=str, choices=['DEBUG', 'INFO', 'ERROR', 'DISABLED'],
                        default='INFO', help='Log level')
    # parser.add_argument('--run-tests', default=True, action=argparse.BooleanOptionalAction)
    parser.add_argument('--run-tests', dest='run_tests', action='store_true')
    parser.add_argument('--no-run-tests', dest='run_tests', action='store_false')
    parser.add_argument('--config', type=str, required=True, help='Config file path')

    args, unknown = parser.parse_known_args()
    log_levels = {'DEBUG': logging.DEBUG, 'INFO': logging.INFO, 'ERROR': logging.ERROR, 'DISABLED': logging.CRITICAL + 1}
    logger.setLevel(level=log_levels[args.log_level])

    if args.run_tests:
        logger.info('Running quick self-test (you can disable them with --no-run-tests)...\n')
        logger.setLevel(logging.CRITICAL)
        ut.main(argv=['first-arg-is-ignored'], exit=False)
        logger.setLevel(level=log_levels[args.log_level])
        logger.info(f'Testing Done\n')


    if not args.config:
        logger.error("No --config has been specified => exiting...")
        print_usage_and_exit(None, None)

    import importlib.util
    config_module_spec = importlib.util.spec_from_file_location("config.py", args.config)
    config = importlib.util.module_from_spec(config_module_spec)
    config_module_spec.loader.exec_module(config)
    config_provider: ConfigProvider = config.ConfigProvider()
    configs: t.Dict[str, Dockerfiles] = config_provider.configs()

    if len(unknown) == 0:
        logger.error("No section of the config has been specified => exiting...")
        print_usage_and_exit(configs, args.config)
    config_name: str = unknown[0]

    if config_name not in configs:
        logger.error(f"There is no {config_name} in {args.config} => exiting...")
        print_usage_and_exit(configs, args.config)

    prefix: str = config_provider.image_prefix()
    logger.info(f'{c.BOLD}Building {c.WARNING}{config_name}{c.ENDC}{c.BOLD} from {c.WARNING}{args.config}{c.ENDC}{c.BOLD} with prefix: '
                f'{c.WARNING}{prefix}{c.ENDC}{c.BOLD} and docker parameters: {c.WARNING}{unknown[1:]}{c.ENDC}{c.BOLD}...{c.ENDC}\n\n')
    builder: Builder = Builder(global_prefix=prefix, dry_run=args.dry_run)
    builder.build(configs=configs, name=config_name, docker_build_opts=unknown[1:])

    print('\n')
    print(f'{c.BOLD}Done{c.ENDC}')
    print(f'Run {c.BOLD}{c.WARNING}docker builder prune -fa && docker image prune -f{c.ENDC} if you wish to clean up images and cache')
    print(f'Run {c.BOLD}{c.WARNING}docker system prune -fa{c.ENDC} if you wish to REMOVE EVERYTHING')


if __name__ == '__main__':
    main()
