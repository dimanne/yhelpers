#!/bin/bash

set -e

if [ "$#" -ne 8 ]; then
    echo "Wrong number of parameters"
    exit 1
fi

while [[ $# -gt 0 ]]
   do
      key="$1"
      case $key in
      --file)
         FILENAME="$2"; shift; shift;
      ;;
      --url)
         URL="$2"; shift; shift;
      ;;
      --checksum_command)
         CHECKSUM_COMMAND="$2"; shift; shift;
      ;;
      --hash)
         HASH="$2"; shift; shift;
      ;;
      *)    # unknown option
         echo "unknown option: $key"
         exit 1
      ;;
   esac
done


echo "Downloading $FILENAME from $URL and verifying with $CHECKSUM_COMMAND against $HASH..."
set -x
wget -q ${URL} -O $FILENAME
echo "$HASH $FILENAME" | $CHECKSUM_COMMAND --check
