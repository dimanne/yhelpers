# ======================================== Header ========================================
function impl_mixin_deps
   set this_dir (dirname (realpath (status current-filename)))
   echo $this_dir/../20_fish/mixins/common_fish_setup.fish
   echo $this_dir/../20_fish/mixins/bat.fish
   echo $this_dir/mixins/docker_basic.fish
   echo $this_dir/mixins/docker_qtcreator.fish
   echo $this_dir/mixins/docker_jetbrains.fish
   echo $this_dir/mixins/yellow.fish
end
function impl_config_fish
end
function impl_fish_home_path
   echo $HOME
end
function impl_on_fish_install
end
# ======================================== Other ========================================



function install-fish
  rm -rf (impl_fish_home_path)/.config/fish
  $HOME/devel/yhelpers/docker/20_fish/install_fish.fish $HOME/devel/yhelpers/docker/fish/env_yellow_laptop.fish
  omf reload
end

function impl_install_fish_for_docker_dev_container
#    set this_dir (dirname (realpath (status current-filename)))
#    if not test -e "$HOME/docker_mounts/yellow_vpn/.config/fish"
#       $HOME/devel/yhelpers/docker/20_fish/install_fish.fish env_dockcont_yellow.fish 2>&1 > /dev/null
#    end
   impl_docker_make_and_print $HOME/docker_mounts/yellow_vpn/.config/fish      $HOME/.config/fish
   impl_docker_make_and_print $HOME/docker_mounts/yellow_vpn/.local/share/fish $HOME/.local/share/fish
end

function dev_qtc_yellow
   impl_dev_qtc yellow/dev_ides_gl_less _yellow --cap-add IPC_LOCK --security-opt seccomp=unconfined \
                $argv \
                (impl_install_fish_for_docker_dev_container)
end


function dev_pycharm
   impl_dev_pycharm dev_gl_less "" (impl_install_fish_for_docker_dev_container)
end


function dev_yellow_testenv
   impl_yellow_testenv yellow/dev_testenv
end
